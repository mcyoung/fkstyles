# Installation

Need to import the css: `@import 'fk_styles';`
If you'd like to also include (and are using bootstrap 3) the bootstrap overrides: `@import 'fk_styles/bootstrap_overrides';`

Need to include the javascript for fixed table headers: `\\= require jquery.floatThead.min`