lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fk_styles/version'

Gem::Specification.new do |s|
  s.name = 'fk_styles'
  s.version = FKStyles::VERSION
  s.date = '2016-09-08'
  s.summary = 'Fireknite Styles'
  s.description = "A simple gem that will be a lot of 'helper styles' for design and development ease."
  s.authors = ["Matt Young"]
  s.email = 'mcyoung86@gmail.com'

  s.add_runtime_dependency 'sass'

  s.files = `git ls-files`.split("\n")
end