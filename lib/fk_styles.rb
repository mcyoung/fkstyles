module FKStyles
  class Engine < ::Rails::Engine
    initializer 'fk_styles.assets.precomplie' do |app|
      %w(stylesheets javascripts).each do |sub|
        app.config.assets.paths << root.join('assets', sub).to_s
      end
    end
  end
end